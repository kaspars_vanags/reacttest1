import { useState } from 'react';
import FormComponent from './FormComponents/FormComponent';
import ListComponent from './ListComponents/ListComponent';

import logo from './logo.svg';
import './App.css';

//let testList = [{"name": "jo", "age": 12}, {"name": "ted", "age": 13}];

function App() {

  const [userList, updateUserList] = useState([]);

  const addNewUser = user => {

    updateUserList(prevGoal => {
      const oldUserList = [...prevGoal];
      oldUserList.unshift({name: user.name, age: user.age});
      return oldUserList;
    });
    
  };

  return (
    <div className="App">
      <FormComponent addNewUser={addNewUser}></FormComponent>
      <ListComponent userList={userList} ></ListComponent>
    </div>
  );
}

export default App;
