import React from 'react';
import styles from './AlertComponent.module.css';

const AlertComponent = (props) => {

    const closeModalWindow = () => {
        alert(1);
    }

    return <div>
        <div className={styles.backgroundLayer}  onClick={closeModalWindow}>
        </div>
        <div className={styles.modal}>
            <div className={styles.modalContent}>
                <div className={styles.modalHeader}>
                    <h1>Error!</h1>
                </div>
                <div className={styles.modalBody}>
                    <h1>{props.message}</h1>
                </div>
                <div className={styles.modalFoot}>
                    <button onClick={closeModalWindow} className={styles.btn}>Close</button>
                </div>
            </div>
        </div>
    </div>;
}

export default AlertComponent;