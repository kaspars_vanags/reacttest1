
import React from 'react';
import { useState } from 'react';
import styles from './FormComponent.module.css';
import AlertComponent from '../WidgetComponents/AlertComponent';

const FormComponent = (props) => {

    let [errorMessage, changeErrorMessage] = useState("");
    const [formObject, changeFormObject] = useState({
        "name" : "Jacob",
        "age" : 0,
        "hasError" : false,
        "errorMessage" : ""
    });

    let createNewUser = () => {
        //first we need to validate user 
        if (formObject.age < 0) {
            errorMessage = <AlertComponent message='Yo, age must be positive number'></AlertComponent>;
            changeErrorMessage(errorMessage);
            return;
        }

        if (formObject.age == "") {
            errorMessage = <AlertComponent message='Please add some age :(('></AlertComponent>;
            changeErrorMessage(errorMessage);
            return;
        }

        if (formObject.name == "") {
            errorMessage = <AlertComponent message='Please enter valid human name :(('></AlertComponent>;
            changeErrorMessage(errorMessage);
            return;
        }

        //todo: some validation
        props.addNewUser(formObject);
    }

    let changeAge = (form) => {
        //console.log(form.target.value);
        changeFormObject(prevUser => {
            prevUser['age'] = form.target.value;
            return prevUser;
        })
    }

    let changeName = (form) => {
        //console.log(form.target.value);
        changeFormObject(prevUser => {
            prevUser['name'] = form.target.value;
            return prevUser;
        })
    }

    let returnErrorBody = (message) => {
        return <div><div className='title-box'></div><div className='body-box'>{message}</div></div>;
    }

    return <div>
        <div>{errorMessage}</div>
        <div className={styles.formGroup}>
            <label htmlFor="name">Name</label>
            <input name="name" onChange={changeName} defaultValue={formObject.name}></input>
        </div>
        <div className={styles.formGroup}>
            <label htmlFor="age">Age</label>
            <input type="number" name="age" onChange={changeAge} defaultValue={formObject.age}></input>
        </div>
        <div className={styles.formGroup}>
            <button onClick={createNewUser}>Add User</button>
        </div>
    </div>;
}

export default FormComponent;